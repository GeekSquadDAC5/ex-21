﻿using System;
using System.Collections.Generic;

namespace ex_21
{
    class Program
    {
        static void Main(string[] args)
        {
            //----------------------------------------
            // Exercise #21 - Creating Loops
            //----------------------------------------
            // Note: You only need a single repository for this task - everything can be coded in a single Program.cs
            // Note 2: When you go on to the next question, watch out for the names of the variables.
            // Note 3: If it helps with your program flow, write out a flow diagram to help you with your program

            int count = 5;
            int i;

            // a) Create a simple for loop that prints out the numbers 1 to 5.
            Console.WriteLine("---------- Task A ----------");

            for(i=0;i<count;i++)
            {
                Console.WriteLine($"Number : {i+1}");
            }

            // b) Create a simple while loop that prints out the numbers 1 to 5.
            Console.WriteLine("\n\n---------- Task B ----------");

            i = 0;
            while(i<count)
            {
                Console.WriteLine($"Number : {i+1}");

                ++i;
            }

            // c) Create a for loop and then a while loop that displays only the even numbers from 1 to 20
            Console.WriteLine("\n\n---------- Task C-1 ----------");

            count = 20;
            for(i=1;i<=count;i++)
            {
                if(i%2 == 0)
                {
                    Console.WriteLine($"Number : {i}");
                }
            }

            Console.WriteLine("\n\n---------- Task C-2 ----------");

            i = 1;
            while(i <= count)
            {
                if(i%2 == 0)
                {
                    Console.WriteLine($"Number : {i}");
                }

                ++i;
            }

            // d) Create a “do while loop” that has an index greater than the counter. Print a line that
            // prints the value of both, indicating that the index is bigger. Note: you will also need to
            // use an if statement.
            Console.WriteLine("\n\n---------- Task D ----------");

            i = 10;
            count = 5;
            do
            {
                Console.WriteLine($"Index   : {i}");
                Console.WriteLine($"Counter : {count}");
                if(i == count)
                {
                    Console.WriteLine($"Index({i}) is the same as counter({count})");
                }
                else if(i>count)
                {
                    Console.WriteLine($"Index({i}) is greater than counter({count})");
                }
                else if(i < count)
                {
                    Console.WriteLine($"Index({i}) is less than counter({count})");
                }
                else
                {
                    Console.WriteLine("Somthing wrong");
                }
            } while(i<5);

            Console.WriteLine("\n\n");
        }
    }
}
